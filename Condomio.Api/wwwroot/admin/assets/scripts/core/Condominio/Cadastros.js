﻿/**
FUNÇOES PARA CADASTROS NA AREA DE CONDOMINIO
AUTOR: LORRAN MOURA "Desenvolver DotNet"
**/
var Cadastros = function () {
    // METADOS
    $('#btncondominiocadastrar').click(function () {
        var nomeok, predominanteok,estadook,cidadeok,bairrook,tipook,enderecook;
        var divresumo = $('#divresumo');
        var divnome = $('#divnome');
        var divendereco = $('#divendereco');
        var divbairro = $('#divbairro');
        var divcidade = $('#divcidade');
        var divestado = $('#divestado');
        var divcep = $('#divcep');
        var divtelefone = $('#divtelefone');
        var divtipo = $('#divtipo');
        var divpredominante = $('#divpredominante');
        var txtresumoCondominio = $('#txtresumoCondominio');
        var txtnomeCondominio = $('#txtnomeCondominio');
        var txtenderecoCondominio = $('#txtenderecoCondominio');
        var txtbairroCondominio = $('#txtbairroCondominio');
        var txtcidadeCondominio = $('#txtcidadeCondominio');
        var cmbestadoCondominio = $('#cmbestadoCondominio');
        var txtcepCondominios = $('#txtcepCondominios');
        var txttelefoneCondominio = $('#txttelefoneCondominio');
        var cmbtipo = $('#cmbtipo');
        var cmbpredominante = $('#cmbpredominante');
        if (txtnomeCondominio.val() === "") {
            divnome.addClass('has-error');
            txtnomeCondominio.pulsate({
                color: "#bf1c56",
                repeat: 4
            });
            nomeok = "0";
        }
        else {
            divnome.removeClass('has-error');
            nomeok = "1";
        }
        if (cmbpredominante.val() === "") {
            divpredominante.addClass('has-error');
            predominanteok = "0";
            cmbpredominante.pulsate({
                color: "#bf1c56",
                repeat: 4
            });
        }
        else {
            divpredominante.removeClass('has-error');
            predominanteok = "1";
        }
        if (cmbestadoCondominio.val() === "") {
            divestado.addClass('has-error');
            estadook = "0";
            cmbestadoCondominio.pulsate({
                color: "#bf1c56",
                repeat: 4
            });
        }
        else {
            divestado.removeClass('has-error');
            estadook = "1";
        }
        if (txtcidadeCondominio.val() === "")
        {
            divcidade.addClass('has-error');
            cidadeok = "0";
            txtcidadeCondominio.pulsate({
                color: "#bf1c56",
                repeat: 4
            });
        }
        else {
            divcidade.removeClass('has-error');
            cidadeok = "1";
        }
        if (txtbairroCondominio.val() === "") {
            divbairro.addClass('has-error');
            bairrook = "0";
            txtbairroCondominio.pulsate({
                color: "#bf1c56",
                repeat: 4
            });
        }
        else {
            divbairro.removeClass('has-error');
            bairrook = "1";
        }
        if (cmbtipo.val() === "") {
            divtipo.addClass('has-error');
            tipook = "0";
            cmbtipo.pulsate({
                color: "#bf1c56",
                repeat: 4
            });
        }
        else {
            divtipo.removeClass('has-error');
            tipook = "1";
        }
        if (txtenderecoCondominio.val() == "") {
            divendereco.addClass('has-error');
            enderecook = "0";
            txtenderecoCondominio.pulsate({
                color: "#bf1c56",
                repeat: 4
            });
        }
        else {
            divendereco.removeClass('has-error');
            enderecook = "1";
        }

        if (nomeok === "0" || predominanteok === "0" || estadook == "0" || cidadeok === "0" || bairrook === "0" || tipook === "0" || enderecook === "0")
        {
            $('#divcamposobrigatorios').removeClass('display-hide');
            $('.go-top').click();
        }
        else if (nomeok === "1" && predominanteok === "1" && estadook == "1" && cidadeok === "1" && bairrook === "1" && tipook === "1" && enderecook === "1")
        {
            $('#divcamposobrigatorios').addClass('display-hide');
            $('.go-top').click();
            alert("ta tudo ok");
        }

        return false;
    })

    return {

        //FUNÇAO PRINCIPAL PARA CHAMAR TODOS OS METADOS CRIADOS
        init: function () {
        },
    };

}();