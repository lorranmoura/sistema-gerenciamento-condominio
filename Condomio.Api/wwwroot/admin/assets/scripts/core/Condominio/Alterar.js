﻿/**
FUNÇOES PARA ALTERAR DADOS DO CONDOMINIO JA CADASTRADO
AUTOR: LORRAN MOURA "Desenvolver DotNet"
**/
var AlterarDadosCondominio = function () {
    // METODOS
    $(window).load(function () {
        $.ajax({
            type: "POST",
            url: "AlterarCondominio.aspx/RertornaNomesCondominio",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msgs) {
                var quantidade = msgs.d.length;
                for (var i = 0 ; i < quantidade; i++) {
                    $("#cmbcondominio").append(new Option(msgs.d[i], msgs.d[i]));
                }
            },
            error: function (msg) {
                alert("erro lorran");
            }
        });
    })
    $('#BtnAltararCondominio').hide();
    $('#cmbcondominio').change(function () {
        var nomecondominio = $('#cmbcondominio').val();
        var txtresumo = $('#txtresumo');
        var txtnome = $('#txtnome');
        var txtendereco = $('#txtendereco');
        var txtbairro = $('#txtbairro');
        var txtcidade = $('#txtcidade');
        var cmbestadoCondominio = $('#cmbestadoCondominio');
        var txtCep = $('#txtCep');
        var txtTelefone = $('#txtTelefone');
        var cmbtipo = $('#cmbtipo');
        var cmbpredominante = $('#cmbpredominante');
        if (!($('#cmbcondominio').val() === "")) {

            $.ajax({
                type: "POST",
                url: "AlterarCondominio.aspx/RertornaDadosCondominioPorNome",
                data: "{nome:'" + nomecondominio + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('#imgcarregando').fadeIn("slow");
                    $('#carregando').fadeIn("slow");
                },
                success: function (msgs) {
                    txtresumo.val(msgs.d[0]);
                    txtnome.val(msgs.d[1]);
                    txtendereco.val(msgs.d[2]);
                    txtbairro.val(msgs.d[3]);
                    txtcidade.val(msgs.d[4]);
                    $("#cmbestadoCondominio option[value='" + msgs.d[5] + "']").attr('selected', true);
                    txtCep.val(msgs.d[6]);
                    txtTelefone.val(msgs.d[7]);
                    $("#cmbtipo option[value='" + msgs.d[8] + "']").attr('selected', true);
                    $("#cmbpredominante option[value='" + msgs.d[9] + "']").attr('selected', true);
                    $('#BtnAltararCondominio').show();
                },
                error: function (msg) {
                    alert("erro lorran");
                },
                complete: function () {
                    $('#imgcarregando').fadeOut("slow");
                    $('#carregando').fadeOut("slow");
                }
            });
        }
        else {
            $('#txtresumo').val("");
            $('#txtnome').val("");
            $('#txtendereco').val("");
            $('#txtbairro').val("");
            $('#txtcidade').val("");
            $('#cmbestadoCondominio').val("");
            $('#txtCep').val("");
            $('#txtTelefone').val("");
            $('#cmbtipo').val("");
            $('#cmbpredominante').val("");
            $('#BtnAltararCondominio').hide();
        }
    })
    $('#BtnAltararCondominio').click(function () {
        return false;
    })
    return {
        //FUNÇAO PRINCIPAL
        init: function () {
        },
    };

}();