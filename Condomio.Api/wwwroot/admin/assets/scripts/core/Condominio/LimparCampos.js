﻿/**
FUNÇÃO PARA LIMPAR TODOS OS CAMPOS NA AREA DE CONDOMINIO
AUTOR: LORRAN MOURA "Desenvolver DotNet"
**/
var LimparCampos = function () {
    // METADOS
    $('#btncondominiolimpar').click(function () {
        $('#txtresumoCondominio').val("");
        $('#txtnomeCondominio').val("");
        $('#txtenderecoCondominio').val("");
        $('#txtbairroCondominio').val("");
        $('#txtcidadeCondominio').val("");
        $('#txtcepCondominios').val("");
        $('#txttelefoneCondominio').val("");
        $('#cmbestadoCondominio').val("");
        $('#cmbtipo').val("");
        $('#cmbpredominante').val("");
        return false;
    })

    return {

        //FUNÇAO PRINCIPAL
        init: function () {
        },
    };

}();