﻿/**
AUTOR: LORRAN MOURA "Desenvolver DotNet"
**/
//VARIAVEIS DO FORUMARIO DE CADASTRO DE UNIDADE
var cmbcondominio = $('#cmbcondominio');
var cmbunidade = $('#txtunidade');
var txtbloco = $('#txtbloco');
var cmbtipounidades = $('#cmbtipounidades');
var cmbgerarconta = $('#cmbgerarconta');
var txtcodlocatario = $('#txtcodlocatario');
var txtnomelocatario = $('#txtnomelocatario');
var btnpesquisalocatario = $('#btnpesquisalocatario');
var txttelefone_locatario = $('#txttelefone_locatario');
var txtcpfcnpj_locatario = $('#txtcpfcnpj_locatario');
var txtcodproprietario = $('#txtcodproprietario');
var txtnomeproprietario = $('#txtnomeproprietario');
var btnpesquisaproprietario = $('#btnpesquisaproprietario');
var txttelefone_proprietario = $('#txttelefone_proprietario');
var txtcpfcnpj_proprietario = $('#txtcpfcnpj_proprietario');
var txtvincularunidade = $('#txtvincularunidade');
var btnpesquisaunidadevincular = $('#btnpesquisaunidadevincular');
var btncadastrarunidade = $('#btncadastrarunidade');
//Função PRINCIPAL
var AlterarUnidade = function () {
    // METADOS
    $(window).load(function () {//FUNÇÃO DE PESQUISA NOMES CONDOMINIO
        $.ajax({
            type: "POST",
            url: "AlterarCondominio.aspx/RertornaNomesCondominio",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msgs) {
                var quantidade = msgs.d.length;
                for (var i = 0 ; i < quantidade; i++) {
                    $("#cmbcondominio").append(new Option(msgs.d[i], msgs.d[i]));
                }
            },
            error: function (msg) {
                alert("Error");
            }
        });
    })
    btnpesquisalocatario.click(function () {//FUNÇÃO DE PESQUISA LOCATÁRIO
        alert("apertou no botão de pesquisa locatário");
        return false;
    })
    btnpesquisaproprietario.click(function () {//FUNÇÃO DE PESQUISA PROPRIETÁRIO
        alert("apertou no botão de pesquisa Proprietário")
        return false;
    });
    btnpesquisaunidadevincular.click(function () {//FUNÇÃO DE VINCULAR UNIDADE
        alert("apertou no botão de vincular unidade")
        return false;
    })
    btncadastrarunidade.click(function () {//FUNÇÃO DE CADASTRA UNIDADE
        alert("ok");
        return false;
    })
    return {
        //FUNÇAO PRINCIPAL
        init: function () {
        },
    };
}();