﻿/**
SEGURANÇA DO SISTEMA NO MOMENTO ESTÁ DESATIVADO
**/
var SegurancaSistema = function () {

    // METADOS
    //VERIFICA SE POSSUIR VARIAVES ATIVAS DE CONEXAO E CONECTSTRING ATIVA
    var Verifica_sepossuir_conectionstring_ativa_ou_banco_ativo = function () {
        $.ajax({
            type: "POST",
            url: "Classes/UsuarioLogado.asmx/consutarusuarioativo",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msgs) {
                if (msgs.d === "0") {
                    $(location).attr('href', '../Login.aspx');
                }
            },
            error: function (msg) {
            }
        });
    }
    return {
        //FUNÇAO PRINCIPAL PARA CHAMAR TODOS OS METADOS CRIADOS
        init: function () {
            Verifica_sepossuir_conectionstring_ativa_ou_banco_ativo();
        },
    };

}();