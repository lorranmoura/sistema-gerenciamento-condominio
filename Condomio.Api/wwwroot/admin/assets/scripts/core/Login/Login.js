﻿var Login = function () {

    var handleLogin = function () {
        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            contentType: " charset=utf-8",
            rules: {
                Empresa: {
                    required: true,
                    number: true
                },
                username: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },
            messages: {
                Empresa: {
                    required: "Código da empresa inválido.",
                    number: "Esse Campo Aceita Somente Numeros"
                },
                username: {
                    required: "Esse Campo e Essencial.",
                    email: "E-mail Inválido."
                },
                password: {
                    required: "A Senha é Obrigatória."
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                $('.alert-warning', $('.login-form')).hide();
                $('.alert-success', $('.login-form')).hide();
                $('.alert-info', $('.login-form')).hide();
                $('.alert-link', $('.login-form')).hide();
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
           
           // submitHandler: function (form) {
        //        var data = 'grant_type=password&name=' + $("#txtusuario").val() + "&password=" + $("#txtsenha").val();
        //        'use strict';
        //        angular.module('SistemaCondominioModule').controller('LoginCtrl', LoginCtrl);
        //        LoginCtrl.$inject = ['$location', '$rootScope', 'SETTINGS'];

        //        function LoginCtrl(SETT)
        //        {
        //        }

        //        $.ajax({
        //            type: "POST",
        //            url: "Login.aspx/login",
        //            data: "{CodEmpresa:'" + $("#txtcodigocondominio").val() + "' ,Username:'" + $("#txtusuario").val() + "',Senha:'" + $("#txtsenha").val() + "' }",
        //            contentType: "application/json; charset=utf-8",
        //            dataType: "json",
        //            success: function (msgs) {
        //                if (msgs.d === "1") {
        //                    $('.alert-danger', $('.login-form')).hide();
        //                    $('.alert-info', $('.login-form')).hide();
        //                    $('.alert-warning', $('.login-form')).hide();
        //                    $('.alert-link', $('.login-form')).hide();
        //                    $('.alert-success', $('.login-form')).show();
        //                    $(location).attr('href', 'Default.aspx');
        //                }
        //                else if (msgs.d === "2") {
        //                    $('.alert-danger', $('.login-form')).hide();
        //                    $('.alert-success', $('.login-form')).hide();
        //                    $('.alert-info', $('.login-form')).hide();
        //                    $('.alert-link', $('.login-form')).hide();
        //                    $('.alert-warning', $('.login-form')).show();
        //                }
        //                else if (msgs.d === "3") {
        //                    $('.alert-danger', $('.login-form')).hide();
        //                    $('.alert-success', $('.login-form')).hide();
        //                    $('.alert-warning', $('.login-form')).hide()
        //                    $('.alert-link', $('.login-form')).hide();
        //                    $('.alert-info', $('.login-form')).show();
        //                }
        //                else {
        //                    $('.alert-danger', $('.login-form')).hide();
        //                    $('.alert-success', $('.login-form')).hide();
        //                    $('.alert-warning', $('.login-form')).hide()
        //                    $('.alert-info', $('.login-form')).hide();
        //                    $('#txtresultadoerro').html(msgs.d);
        //                    $('.alert-link', $('.login-form')).show();
        //                }
        //            },
        //            error: function (msg) {
        //            }
        //        });
         //  }
        });

        $('.login-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit();
                }
                return false;
            }
        });
    }

  
    var handleForgetPassword = function () {
        $('.forget-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                Empresa: {
                    required: true,
                    number: true
                },
                email: {
                    required: true,
                    email: true
                }
            },

            messages: {
                Empresa: {
                    required: "O Campo Empresa e  Essencial.",
                    number: "Esse Campo Aceita Somente Numeros"
                },
                email: {
                    required: "O Email e Requerido.",
                    email: "Por favor insira um endere�o de e-mail v�lido"
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   

            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            //submitHandler: function (form) {
            //    form.submit();
            //}
        });

        $('.forget-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    $('.forget-form').submit();
                }
                return false;
            }
        });

        jQuery('#forget-password').click(function () {
            jQuery('.login-form').hide();
            jQuery('.forget-form').show();
        });

        jQuery('#back-btn').click(function () {
            jQuery('.login-form').show();
            jQuery('.forget-form').hide();
        });

    }



    return {
        //main function to initiate the module
        init: function () {

            handleLogin();
            handleForgetPassword();
            $.backstretch([
		        "admin/assets/img/bg/1f.jpg",
		        "admin/assets/img/bg/2f.jpg",
		        "admin/assets/img/bg/3f.jpg",
                "admin/assets/img/bg/5f.jpg"
            ], {
                fade: 1000,
                duration: 8000
            });
        }

    };

}();