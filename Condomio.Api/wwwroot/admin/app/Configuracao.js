﻿(function () {
    'use strict';
    angular.module("SistemaCondominioModule").constant('SETTINGS', {
        'VERSION': '0.0.1',
        'CURR_ENV': 'admin',
        'AUTH_TOKEN': 'SistemaCondominio-token',
        'AUTH_USER': 'SistemaCondominio-user',
        'AUTH_EMPRESA': 'SistemaCondominio-empresa',
        'SERVICE_URL': '/'
        //'SERVICE_URL': 'http://minhaapi.azurewebsites.net/',
    });
    angular.module("SistemaCondominioModule").run(function ($rootScope, $location, SETTINGS) {
        var token = localStorage.getItem(SETTINGS.AUTH_TOKEN);
        var user = localStorage.getItem(SETTINGS.AUTH_USER);
        var empresa = localStorage.getItem(SETTINGS.AUTH_EMPRESA);

        $rootScope.user = null;
        $rootScope.token = null;
        $rootScope.Empresa = null;
        $rootScope.header = null;

        if (token && user) {
            $rootScope.user = user;
            $rootScope.token = token;
            $rootScope.Empresa = empresa;
            $rootScope.header = { headers: { 'Authorization': 'Bearer' + $rootScope.token } }
        }
        $rootScope.$on("$routeChangeStart", function (event, proximaRota, rotaAtual) {
            if ($rootScope.user == null) {
                window.location.href = "../Login.html";
            }
        });
    });
})();