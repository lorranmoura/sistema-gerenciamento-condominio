﻿(function () {
    'use strict';
    angular.module('SistemaCondominioModule').config(function ($routeProvider) {
        $routeProvider.when('/', { controller: 'HomeCtrl', controllerAs: 'vm', templateUrl: 'Pages/Home/Index.html' })
            .when('/login', {
                controller: 'LoginCtrl',
                controllerAs: 'vm',
                templateUrl: 'Pages/SharedExit/Index.html'
            })
            .when('/logout', {
                controller: 'LogoutCtrl',
                controllerAs: 'vm',
                templateUrl: 'Pages/SharedExit/Index.html'
            }).otherwise({ redirectTo: '/' });;
    });
})();