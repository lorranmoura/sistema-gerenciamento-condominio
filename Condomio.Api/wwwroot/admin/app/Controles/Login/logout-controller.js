﻿(function () {
    'user strict';
    function logoutCtrl($rootScope, $location, SETTINGS) {
        var vm = this;
        activate();
        function activate() {
            $rootScope.user = null;
            $rootScope.token = null;
            localStorage.removeItem(SETTINGS.AUTH_TOKEN);
            localStorage.removeItem(SETTINGS.AUTH_USER);
            window.location = "../Login.html";
        }
    }

    angular.module('SistemaCondominioModule').controller('LogoutCtrl', logoutCtrl);
    logoutCtrl.$inject = ['$rootScope', '$location', 'SETTINGS'];
    //$rootScope.user = null;
    //$rootScope.token = null;
    //localStorage.removeItem(SETTINGS.AUTH_TOKEN);
    //localStorage.removeItem(SETTINGS.AUTH_USER);
    //window.location = "../Login.html";
    //activate();

    //function activate() {

})();