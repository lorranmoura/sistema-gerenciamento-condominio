﻿(function () {
    //'use strict';
    var app = angular.module('SistemaCondominioModule').controller('LoginController', LoginCtrl);
    LoginCtrl.$inject = ['$scope', '$location', '$rootScope', 'SETTINGS', 'AccountFactory'];
    function LoginCtrl($scope, $location, $rootScope, SETTINGS, AccountFactory) {
        $scope.login = { email: '', empresa: '', password: '' }
        $scope.submit = function () {
            if ($scope.login != null) {
                function success(response) {
                    console.log(response);
                    $rootScope.user = $scope.login.email;
                    $rootScope.token = response.access_token;
                    localStorage.setItem(SETTINGS.AUTH_TOKEN, response.access_token);
                    localStorage.setItem(SETTINGS.AUTH_USER, $rootScope.user);
                    window.location = "admin/Index.html";
                }
                function fail(error) {
                    toastr.error(error.data.error_description, 'Falha na autenticação');
                }
                AccountFactory.login($scope.login).success(success).catch(fail);
            }
        };
    };
})();

//(function () {
//    //'use strict';
//    var app = angular.module('SistemaCondominioModule').controller('ExampleController', LoginCtrl);
//    LoginCtrl.$inject = ['$scope', '$http', '$location', '$rootScope', 'SETTINGS'];
//    function LoginCtrl($scope, $http, $location, $rootScope, SETTINGS) {
//        $scope.login = { email: '', empresa: '', password: '' }
//        $scope.submit = function () {
//            if ($scope.login != null) {
//                var data = "grant_type=password&username=" + $scope.login.email + "&password=" + $scope.login.password;
//                var url = SETTINGS.SERVICE_URL + 'api/security/token';
//                var header = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } };
//                function success(response) {
//                    console.log(response);
//                    $rootScope.user = $scope.login.email;
//                    $rootScope.token = response.access_token;
//                    localStorage.setItem(SETTINGS.AUTH_TOKEN, response.access_token);
//                    localStorage.setItem(SETTINGS.AUTH_USER, $rootScope.user);
//                    window.location = "admin/Index.html";
//                }
//                function fail(error) {
//                    toastr.error(error.data.error_description, 'Falha na autenticação');
//                }
//                $http.post(url, data, header).success(success).catch(fail);
//            }
//        };
//    };
//})();