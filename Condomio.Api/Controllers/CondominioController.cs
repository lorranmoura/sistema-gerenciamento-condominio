﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using SistemaGerenciamentoCond.Application.Interface;
using SistemaGerenciamentoCondominio.Dominio.Enum;
namespace Condomio.Api.Controllers
{
    public class CondominioController : BaseController
    {
        private readonly ICondominioAppService _condominioAppService;

        public CondominioController(ICondominioAppService appService)
        {
            _condominioAppService = appService;
        }
        [HttpPost]
        [Route("api/condominio")]
        public Task<HttpResponseMessage> Post([FromBody]dynamic body)
        {
            var condominio = new SistemaGerenciamentoCondominio.Dominio.Entities.Condominio(
                nomeCondominio: (string)body.condominio,
                endereco: "dfsdfsdf",
                cidade: "DFdgdfg",
                estado: "dsf",
                predominante: "dsfsd",
                bairro: "sdfsdf",
                tipoCondominio: TipoCondominio.Horizontal,
                telefone: "dsfsdf",
                cnpj: "sdfsd",
                resumo: "sadasd"
            );
            var user = _condominioAppService.CadastrarCondominio(condominio);
            return CreateResponse(HttpStatusCode.Created, user);
        }
    }
}