﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using SistemaGerenciamentoCond.Application.Interface;
using SistemaGerenciamentoCondominio.Dominio.Entities;
using SistemaGerenciamentoCondominio.Dominio.Enum;

namespace Condomio.Api.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserAppService _userAppService;

        public UserController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }
        [HttpPost]
        [Route("api/users")]
        public Task<HttpResponseMessage> Post([FromBody]dynamic body)
        {
            var command = new User(nome: (string)body.nome, passaword: (string)body.password, condominioId: 5, email: (string)body.email,
                date: DateTime.Now, pessoaLocatarioProprietarioId: 1, tipoUser: TipoUser.Administrador);

            var user = _userAppService.CadastrarUser(command);

            return CreateResponse(HttpStatusCode.Created, user);
        }

        [HttpGet]
        [Route("api/users")]
        [Authorize]
        public string nome()
        {
            return User.Identity.Name;
        }
    }
}