﻿using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;
using SistemaGerenciamentoCond.Application.Interface;
using SistemaGerenciamentoCondominio.Dominio.Enum;

namespace Condomio.Api.Security
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        readonly IUserAppService _userService;

        public SimpleAuthorizationServerProvider(IUserAppService userService)
        {
            _userService = userService;
        }

#pragma warning disable 1998
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
#pragma warning restore 1998
        {
            context.Validated();
        }

#pragma warning disable 1998
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
#pragma warning restore 1998
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var user = _userService.Autenticacao(context.UserName, context.Password);
            if (user == null)
            {
                context.SetError("invalid_grant", "Usuário ou senha inválidos");
                return;
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            identity.AddClaim(new Claim(ClaimTypes.Name, user.Email));
            identity.AddClaim(new Claim(ClaimTypes.Role, user.TipoUser == TipoUser.Administrador ? "admin" : ""));

            GenericPrincipal principal = new GenericPrincipal(identity, new[] { user.TipoUser == TipoUser.Administrador ? "admin" : "" });
            Thread.CurrentPrincipal = principal;
            context.Validated(identity);
        }
    }
}