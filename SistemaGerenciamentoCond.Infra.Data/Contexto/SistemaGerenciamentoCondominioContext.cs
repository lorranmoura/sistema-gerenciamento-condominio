﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using SistemaGerenciamentoCond.Infra.Data.EntityConfig;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.Contexto
{
    public class SistemaGerenciamentoCondominioContext : DbContext
    {
        public SistemaGerenciamentoCondominioContext()
            : base(@"Data Source=C:\Dev\DBCondominio\dbCondominio.sdf;Max Database Size=4091")
        { }
        public DbSet<Condominio> Condominio { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Unidade> Unidade { get; set; }
        public DbSet<Baixa> Baixa { get; set; }
        public DbSet<Boletos> Boletos { get; set; }
        public DbSet<ContaCorrente> ContaCorrente { get; set; }
        public DbSet<ContasPagar> ContasPagar { get; set; }
        public DbSet<ContasReceber> ContasReceber { get; set; }
        public DbSet<Fornecedor> Fornecedor { get; set; }
        public DbSet<ModuloSistema> ModuloSistema { get; set; }
        public DbSet<Morador> Morador { get; set; }
        public DbSet<Notificacao> Notificacao { get; set; }
        public DbSet<NotificacaoAll> NotificacaoAll { get; set; }
        public DbSet<PessoaLocatarioProprietario> PessoaLocatarioProprietario { get; set; }
        public DbSet<PlanoDeContas> PlanoDeContas { get; set; }
        public DbSet<Representantes> Representantes { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<SmsCobranca> SmsCobranca { get; set; }
        public DbSet<UserRoles> UserRoles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Properties().Where(p => p.ReflectedType != null && p.Name == p.ReflectedType.Name + "Id").Configure(p => p.IsKey());
            modelBuilder.Properties<string>().Configure(p => p.HasColumnType("nvarchar"));
            modelBuilder.Properties<string>().Configure(p => p.HasMaxLength(200));
            //CONFIGURAÇÃO CLASS SISTEMA BANCO PERSONALIZADA
            modelBuilder.Configurations.Add(new CondominioConfiguration());
            modelBuilder.Configurations.Add(new PessoaLocatarioProprietarioConfiguration());
            modelBuilder.Configurations.Add(new UnidadeConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new UserRolesConfiguration());
            modelBuilder.Configurations.Add(new SmsCobrancaConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new RepresentantesConfiguration());
            modelBuilder.Configurations.Add(new PlanoDeContasConfiguration());
            modelBuilder.Configurations.Add(new NotificacaoAllConfiguration());
            modelBuilder.Configurations.Add(new NotificacaoConfiguration());
            modelBuilder.Configurations.Add(new MoradorConfiguration());
            modelBuilder.Configurations.Add(new ModuloSistemaRolesConfiguration());
            modelBuilder.Configurations.Add(new FornecedorConfiguration());
            modelBuilder.Configurations.Add(new ContasReceberConfiguration());
            modelBuilder.Configurations.Add(new ContasPagarConfiguration());
            modelBuilder.Configurations.Add(new ContaCorrenteConfiguration());
            modelBuilder.Configurations.Add(new BoletosConfiguration());
            modelBuilder.Configurations.Add(new BaixaConfiguration());
        }
    }
}
