﻿using SistemaGerenciamentoCondominio.Dominio.Entities;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories;

namespace SistemaGerenciamentoCond.Infra.Data.Repositories
{
    //IMPLEMENTAÇÃO ESPECIFICA PARA CONDOMINIO CLASS---ICondominioRepository JÁ POSSUIR O CRUD PADRAO
    public class CondominioRepository : RepositoryBase<Condominio>, ICondominioRepository
    {
    }
}
