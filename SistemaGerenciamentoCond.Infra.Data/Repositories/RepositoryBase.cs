﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SistemaGerenciamentoCond.Infra.Data.Contexto;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories;

namespace SistemaGerenciamentoCond.Infra.Data.Repositories
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected SistemaGerenciamentoCondominioContext Db = new SistemaGerenciamentoCondominioContext();
        public void Adcionar(TEntity obj)
        {
            Db.Set<TEntity>().Add(obj);
            Db.SaveChanges();
        }

        public TEntity BuscaPorId(int id)
        {
            return Db.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> BuscarTodos()
        {
            return Db.Set<TEntity>().ToList();
        }

        public void Atualizar(TEntity obj)
        {
            Db.Entry(obj).State= EntityState.Modified;
            Db.SaveChanges();
        }

        public void Remover(TEntity obj)
        {
            Db.Set<TEntity>().Remove(obj);
            Db.SaveChanges();
        }

        public void Dispose()
        {
           
        }
    }
}
