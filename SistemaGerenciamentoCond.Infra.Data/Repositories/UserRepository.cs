﻿using System.Linq;
using SistemaGerenciamentoCondominio.Dominio.Entities;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories;
using SistemaGerenciamentoCondominio.Dominio.Specs;

namespace SistemaGerenciamentoCond.Infra.Data.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public User Autenticacao(string nome, string password)
        {
            return Db.User
                .Where(UserSpecs.AuthenticateUser(nome, password))
                .FirstOrDefault();
        }
    }
}
