﻿using System.Data.Entity.ModelConfiguration;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class NotificacaoAllConfiguration : EntityTypeConfiguration<NotificacaoAll>
    {
        public NotificacaoAllConfiguration()
        {
            HasKey(x => x.NotificacaoAllId);
            HasRequired(x => x.User).WithMany().HasForeignKey(x => x.UserId);
            HasRequired(x => x.Notificacao).WithMany().HasForeignKey(x => x.NotificacaoId);
        }
    }
}
