﻿using System.Data.Entity.ModelConfiguration;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class PlanoDeContasConfiguration : EntityTypeConfiguration<PlanoDeContas>
    {
        public PlanoDeContasConfiguration()
        {
            HasKey(x => x.PlanoDeContasId);
            HasRequired(x => x.Condominio).WithMany().HasForeignKey(x => x.CondominioId);
            Property(x => x.NomeItem).IsRequired().HasMaxLength(80);
        }
    }
}
