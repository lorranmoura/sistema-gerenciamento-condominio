﻿using System.Data.Entity.ModelConfiguration;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class ContasReceberConfiguration : EntityTypeConfiguration<ContasReceber>
    {
        public ContasReceberConfiguration()
        {
            HasKey(x => x.ContasReceberId);
            HasRequired(x => x.Condominio).WithMany().HasForeignKey(x => x.CondominioId);
            HasRequired(x => x.PessoaLocatarioProprietario)
                .WithMany()
                .HasForeignKey(x => x.PessoaLocatarioProprietarioId);
            HasRequired(x => x.PlanoDeContas).WithMany().HasForeignKey(x => x.PlanoDeContasId);
            HasRequired(x => x.Unidade).WithMany().HasForeignKey(x => x.UnidadeId);
        }
    }
}
