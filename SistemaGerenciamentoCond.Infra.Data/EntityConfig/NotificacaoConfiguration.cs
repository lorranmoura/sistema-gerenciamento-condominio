﻿using System.Data.Entity.ModelConfiguration;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
  public  class NotificacaoConfiguration:EntityTypeConfiguration<Notificacao>
    {
      public NotificacaoConfiguration()
      {
          HasKey(x => x.NotificacaoId);
      }
    }
}
