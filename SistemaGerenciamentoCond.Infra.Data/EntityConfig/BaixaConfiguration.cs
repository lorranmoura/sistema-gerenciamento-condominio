﻿using System.Data.Entity.ModelConfiguration;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class BaixaConfiguration : EntityTypeConfiguration<Baixa>
    {
        public BaixaConfiguration()
        {
            HasKey(x => x.BaixaId);
            HasRequired(x => x.Condominio).WithMany().HasForeignKey(x => x.CondominioId);
            HasRequired(x => x.Boletos).WithMany().HasForeignKey(x => x.BoletosId);
            HasRequired(x => x.PessoaLocatarioProprietario)
                .WithMany()
                .HasForeignKey(x => x.PessoaLocatarioProprietarioId);
            HasRequired(x => x.Unidade).WithMany().HasForeignKey(x => x.UnidadeId);
        }
    }
}
