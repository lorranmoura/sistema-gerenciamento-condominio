﻿using System.Data.Entity.ModelConfiguration;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class CondominioConfiguration : EntityTypeConfiguration<Condominio>
    {
        public CondominioConfiguration()
        {
            HasKey(c => c.CondominioId);
            Property(c => c.NomeCondominio).IsRequired().HasMaxLength(255);
            Property(c => c.Predominante).IsRequired().HasMaxLength(255);
            //Property(c => c.TipoCondominio).IsRequired().HasMaxLength(80);
            Property(c => c.Estado).IsRequired().HasMaxLength(100);
            Property(c => c.Cidade).IsRequired().HasMaxLength(100);
            Property(c => c.Bairro).IsRequired().HasMaxLength(100);

        }
    }
}
