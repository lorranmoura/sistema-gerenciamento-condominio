﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class UserRolesConfiguration : EntityTypeConfiguration<UserRoles>
    {
        public UserRolesConfiguration()
        {
            HasKey(x => x.UserRolesId);
            HasRequired(x => x.Condominio).WithMany().HasForeignKey(x => x.CondominioId);
            HasRequired(x => x.User).WithMany().HasForeignKey(x => x.UserId);
            HasRequired(x => x.Role).WithMany().HasForeignKey(x => x.RoleId);

        }
    }
}
