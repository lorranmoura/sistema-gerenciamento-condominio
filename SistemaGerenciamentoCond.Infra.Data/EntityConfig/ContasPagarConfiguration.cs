﻿using System.Data.Entity.ModelConfiguration;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class ContasPagarConfiguration : EntityTypeConfiguration<ContasPagar>
    {
        public ContasPagarConfiguration()
        {
            HasKey(x => x.ContasPagarId);
            HasRequired(x => x.Condominio).WithMany().HasForeignKey(x => x.CondominioId);
            HasRequired(x => x.Fornecedor).WithMany().HasForeignKey(x => x.FornecedorId);
            HasRequired(x => x.PlanoDeContas).WithMany().HasForeignKey(x => x.PlanoDeContasId);
        }
    }
}
