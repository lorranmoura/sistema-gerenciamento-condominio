﻿using System.Data.Entity.ModelConfiguration;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class BoletosConfiguration : EntityTypeConfiguration<Boletos>
    {
        public BoletosConfiguration()
        {
            HasKey(x => x.BoletosId);
            HasRequired(x => x.PessoaLocatarioProprietario)
                .WithMany()
                .HasForeignKey(x => x.PessoaLocatarioProprietarioId);
            HasRequired(x => x.Unidade).WithMany().HasForeignKey(x => x.UnidadeId);
        }
    }
}
