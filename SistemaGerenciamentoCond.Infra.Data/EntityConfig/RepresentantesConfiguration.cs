﻿
using System.Data.Entity.ModelConfiguration;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class RepresentantesConfiguration:EntityTypeConfiguration<Representantes>
    {
        public RepresentantesConfiguration()
        {
            HasKey(x => x.RepresentantesId);
            HasRequired(x => x.Condominio).WithMany().HasForeignKey(x => x.CondominioId);
        }
    }
}
