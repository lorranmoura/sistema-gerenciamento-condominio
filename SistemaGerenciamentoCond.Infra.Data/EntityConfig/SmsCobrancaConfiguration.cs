﻿
using System.Data.Entity.ModelConfiguration;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class SmsCobrancaConfiguration : EntityTypeConfiguration<SmsCobranca>
    {
        public SmsCobrancaConfiguration()
        {
            HasKey(x => x.SmsCobrancaId);
            Property(x => x.Nome).IsRequired();
            HasRequired(x => x.Condominio).WithMany().HasForeignKey(x => x.CondominioId);
        }
    }
}
