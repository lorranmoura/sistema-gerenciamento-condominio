﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class PessoaLocatarioProprietarioConfiguration : EntityTypeConfiguration<PessoaLocatarioProprietario>
    {
        public PessoaLocatarioProprietarioConfiguration()
        {
            HasKey(x => x.PessoaLocatarioProprietarioId);
            HasRequired(x => x.Condominio).WithMany().HasForeignKey(x => x.CondominioId);
            Property(x => x.NomePessoa).IsRequired().HasMaxLength(190);


        }
    }
}
