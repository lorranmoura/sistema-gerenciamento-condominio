﻿using System.Data.Entity.ModelConfiguration;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            HasKey(x => x.UserId);
            HasRequired(x => x.Condominio).WithMany().HasForeignKey(x => x.CondominioId);
            HasRequired(x => x.PessoaLocatarioProprietario).WithMany().HasForeignKey(x => x.PessoaLocatarioProprietarioId);
            Property(x => x.Nome).IsRequired();
            Property(x => x.Passaword).IsRequired();
        }
    }
}
