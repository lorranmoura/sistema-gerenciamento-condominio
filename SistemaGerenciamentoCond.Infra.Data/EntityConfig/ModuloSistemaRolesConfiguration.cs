﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class ModuloSistemaRolesConfiguration : EntityTypeConfiguration<ModuloSistemaRoles>
    {
        public ModuloSistemaRolesConfiguration()
        {
            HasKey(x => x.ModuloSistemaRolesId);
            HasRequired(x => x.ModuloSistema).WithMany().HasForeignKey(x => x.ModuloSistemaId);
            HasRequired(x => x.Role).WithMany().HasForeignKey(x => x.RoleId);
        }
    }
}
