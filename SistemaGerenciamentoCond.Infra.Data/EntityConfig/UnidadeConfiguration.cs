﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class UnidadeConfiguration : EntityTypeConfiguration<Unidade>
    {
        public UnidadeConfiguration()
        {
            HasKey(x => x.UnidadeId);
            Property(x => x.TipoUnidade).IsRequired().HasMaxLength(100);
            HasRequired(x => x.Condominio).WithMany().HasForeignKey(x => x.CondominioId);
            HasRequired(x => x.PessoaLocatarioProprietario)
                .WithMany()
                .HasForeignKey(x => x.PessoaLocatarioProprietarioId);
        }
    }
}
