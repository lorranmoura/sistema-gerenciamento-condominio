﻿using System.Data.Entity.ModelConfiguration;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class MoradorConfiguration : EntityTypeConfiguration<Morador>
    {
        public MoradorConfiguration()
        {
            HasKey(x => x.MoradorId);
            HasRequired(x => x.Condominio).WithMany().HasForeignKey(x => x.CondominioId);
            HasRequired(x => x.PessoaLocatarioProprietario)
                .WithMany()
                .HasForeignKey(x => x.PessoaLocatarioProprietarioId);
        }
    }
}
