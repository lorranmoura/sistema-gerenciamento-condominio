﻿using System.Data.Entity.ModelConfiguration;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Infra.Data.EntityConfig
{
    public class ContaCorrenteConfiguration : EntityTypeConfiguration<ContaCorrente>
    {
        public ContaCorrenteConfiguration()
        {
            HasKey(x => x.ContaCorrenteId);
            HasRequired(x => x.Condominio).WithMany().HasForeignKey(x => x.CondominioId);
            Property(x => x.Banco).IsRequired();
        }
    }
}
