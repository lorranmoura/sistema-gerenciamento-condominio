﻿using System.Configuration;
using System.Web.Mvc;

namespace SistemaGerenciamentoCond.MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            var connString =
            ConfigurationManager.ConnectionStrings["ConexaoMySQL"].ConnectionString.ToString(); ;
            ViewBag.Message = "Lorrannnn " + connString;

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}