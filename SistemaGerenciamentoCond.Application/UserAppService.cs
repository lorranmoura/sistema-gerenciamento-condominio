﻿using SistemaGerenciamentoCond.Application.Interface;
using SistemaGerenciamentoCondominio.Dominio.Entities;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Services;

namespace SistemaGerenciamentoCond.Application
{
    public class UserAppService : AppServiceBase<User>, IUserAppService
    {
        private readonly IUserService _userService;
        public UserAppService(IUserService serviceBase) : base(serviceBase)
        {
            _userService = serviceBase;
        }

        public User Autenticacao(string userName, string password)
        {
            return _userService.Autenticacao(userName, password);
        }

        public User CadastrarUser(User user)
        {
            user.ValidarUser();
            if (Commit())
            {
                _userService.Adcionar(user);
                return user;
            }
            return null;
        }
    }
}
