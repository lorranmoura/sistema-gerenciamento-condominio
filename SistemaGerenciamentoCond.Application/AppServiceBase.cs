﻿using System;
using Condominio.SharedKernel;
using Condominio.SharedKernel.Events;
using SistemaGerenciamentoCond.Application.Interface;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Services;

namespace SistemaGerenciamentoCond.Application
{
    public class AppServiceBase<TEntity> : IDisposable, IAppServiceBase<TEntity> where TEntity : class
    {
        private readonly IServiceBase<TEntity> _serviceBase;
        private readonly IHandler<DomainNotification> _notifications;
        public AppServiceBase(IServiceBase<TEntity> serviceBase)
        {
            _serviceBase = serviceBase;
            _notifications = DomainEvent.Container.GetService<IHandler<DomainNotification>>();
        }

        public void Adcionar(TEntity obj)
        {
            _serviceBase.Adcionar(obj);
        }

        public TEntity BuscaPorId(int id)
        {
            return _serviceBase.BuscaPorId(id);
        }

        public System.Collections.Generic.IEnumerable<TEntity> BuscarTodos()
        {
            return _serviceBase.BuscarTodos();
        }

        public void Atualizar(TEntity obj)
        {
            _serviceBase.Atualizar(obj);
        }

        public void Remover(TEntity obj)
        {
            _serviceBase.Remover(obj);
        }

        public void Dispose()
        {
            _serviceBase.Dispose();
        }
        public bool Commit()
        {
            if (_notifications.HasNotifications())
                return false;
            return true;

        }
    }
}
