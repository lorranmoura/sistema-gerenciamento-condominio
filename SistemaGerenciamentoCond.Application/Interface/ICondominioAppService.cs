﻿namespace SistemaGerenciamentoCond.Application.Interface
{
    public interface ICondominioAppService : IAppServiceBase<SistemaGerenciamentoCondominio.Dominio.Entities.Condominio>
    {
        SistemaGerenciamentoCondominio.Dominio.Entities.Condominio CadastrarCondominio(SistemaGerenciamentoCondominio.Dominio.Entities.Condominio condominio);
    }
}
