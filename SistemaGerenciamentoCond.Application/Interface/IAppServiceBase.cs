﻿
using System.Collections.Generic;

namespace SistemaGerenciamentoCond.Application.Interface
{
    public interface IAppServiceBase<TEntity> where TEntity : class
    {
        void Adcionar(TEntity obj);
        TEntity BuscaPorId(int id);
        IEnumerable<TEntity> BuscarTodos();
        void Atualizar(TEntity obj);
        void Remover(TEntity obj);
        void Dispose();
    }
}
