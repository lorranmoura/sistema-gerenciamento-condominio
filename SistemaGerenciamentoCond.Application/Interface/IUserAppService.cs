﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCond.Application.Interface
{
    public interface IUserAppService : IAppServiceBase<User>
    {
        User Autenticacao(string email, string password);
        User CadastrarUser(User user);
    }
}
