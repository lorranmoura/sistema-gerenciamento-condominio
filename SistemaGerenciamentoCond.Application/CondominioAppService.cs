﻿using SistemaGerenciamentoCond.Application.Interface;
using SistemaGerenciamentoCondominio.Dominio.Enum;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Services;
namespace SistemaGerenciamentoCond.Application
{
    public class CondominioAppService : AppServiceBase<SistemaGerenciamentoCondominio.Dominio.Entities.Condominio>, ICondominioAppService
    {
        private readonly ICondominioService _condominioService;
        public CondominioAppService(ICondominioService condominioService)
            : base(condominioService)
        {
            _condominioService = condominioService;
        }
        public SistemaGerenciamentoCondominio.Dominio.Entities.Condominio CadastrarCondominio(SistemaGerenciamentoCondominio.Dominio.Entities.Condominio command)
        {
            var condominio = new SistemaGerenciamentoCondominio.Dominio.Entities.Condominio(command.NomeCondominio, "sdfsdf", "sdfsdf", "sdfsdf", "sdfsdf", "sdfsdf", TipoCondominio.Vertical, "sdfsdf", "sdfsdf", "dfssf");
            condominio.ValidarCondominio();
            if (Commit())
            {
                _condominioService.Adcionar(condominio);
                return condominio;
            }
            return null;
        }
    }
}
