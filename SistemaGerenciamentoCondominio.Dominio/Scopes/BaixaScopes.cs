﻿using System.Globalization;
using Condominio.SharedKernel.Validation;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Scopes
{
    public static class BaixaScopes
    {
        public static bool BaixaScopeIsValid(this Baixa baixa)
        {
            return AssertionConcern.IsSatisfiedBy(
                AssertionConcern.AssertNotEmpty(baixa.Sacado, "O sacado é obrigatório"),
                AssertionConcern.AssertNotEmpty(baixa.DataBaixa.ToString(CultureInfo.InvariantCulture), "A data é obrigatória"),
                AssertionConcern.AssertNotEmpty(baixa.CondominioId.ToString(), "O condominio e obrigatório"),
                AssertionConcern.AssertNotEmpty(baixa.Valor.ToString(CultureInfo.InvariantCulture), "O valor é obrigatório"));
        }

    }
}
