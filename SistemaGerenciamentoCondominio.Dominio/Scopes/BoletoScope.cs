﻿using Condominio.SharedKernel.Validation;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Scopes
{
    public static class BoletoScope
    {
        public static bool BoletoIsValid(this Boletos boletos)
        {
            return AssertionConcern.IsSatisfiedBy(
                AssertionConcern.AssertNotEmpty(boletos.Sacado, "O sacado é Obrigatório"),
                AssertionConcern.AssertNotEmpty(boletos.Status.ToString(), "O status é Obrigatório"));
        }
    }
}
