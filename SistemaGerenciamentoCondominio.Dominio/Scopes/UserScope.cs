﻿
using Condominio.SharedKernel.Validation;
using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Scopes
{
    public static class UserScope
    {
        public static bool UserIsValid(this User user)
        {
            return AssertionConcern.IsSatisfiedBy(
                AssertionConcern.AssertNotEmpty(user.Nome, "Nome de Usuário é Obrigatório"),
                AssertionConcern.AssertNotEmpty(user.Passaword, "Senha é Obrigatória"),
                //AssertionConcern.AssertLength(user.Passaword, 6, 20, "A senha deve conter enter 6 a 20 caracteres"),
                AssertionConcern.AssertNotEmpty(user.Email, "Email é obrigatório"));
        }
    }
}
