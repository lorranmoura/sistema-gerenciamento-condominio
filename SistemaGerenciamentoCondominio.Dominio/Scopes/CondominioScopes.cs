﻿using Condominio.SharedKernel.Validation;

namespace SistemaGerenciamentoCondominio.Dominio.Scopes
{
    public static class CondominioScopes
    {
        public static bool CondominioIsValid(this Entities.Condominio condominio)
        {
            return
                AssertionConcern.IsSatisfiedBy(
                    AssertionConcern.AssertNotEmpty(condominio.NomeCondominio, "O nome do condominio e obrigatório"),
                    AssertionConcern.AssertNotEmpty(condominio.Endereco, "O endereco do condominio e obrigatório"),
                    AssertionConcern.AssertNotEmpty(condominio.Cidade, "A cidade é obrigatório"),
                    AssertionConcern.AssertNotEmpty(condominio.Estado, "O estado é obrigatório"),
                    AssertionConcern.AssertNotEmpty(condominio.Bairro, "O bairro é obrigatório"),
                    AssertionConcern.AssertNotEmpty(condominio.Predominante, "O predominante é obrigatório"),
                    AssertionConcern.AssertNotEmpty(condominio.Estado, "O estado é obrigatório"));
        }

        public static bool UpdateCondominioIsValid(this Entities.Condominio condominio)
        {
            return
                AssertionConcern.IsSatisfiedBy(
                    AssertionConcern.AssertNotEmpty(condominio.NomeCondominio, "O nome do condominio e obrigatório"),
                    AssertionConcern.AssertNotEmpty(condominio.Endereco, "O endereco do condominio e obrigatório"),
                    AssertionConcern.AssertNotEmpty(condominio.Cidade, "A cidade é obrigatório"),
                    AssertionConcern.AssertNotEmpty(condominio.Estado, "O estado é obrigatório"),
                    AssertionConcern.AssertNotEmpty(condominio.Bairro, "O bairro é obrigatório"),
                    AssertionConcern.AssertNotEmpty(condominio.Predominante, "O predominante é obrigatório"),
                    AssertionConcern.AssertNotEmpty(condominio.Estado, "O estado é obrigatório"));
        }
    }
}
