﻿
namespace SistemaGerenciamentoCondominio.Dominio.Enum
{
    public enum TipoCondominio
    {
        Horizontal,
        Vertical
    }
}
