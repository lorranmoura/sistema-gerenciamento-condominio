﻿
using System.Collections;
using System.Collections.Generic;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class Role
    {
        public Role(string nomeRole)
        {
            RoleId = 0;
            NomeRole = nomeRole;
        }

        public int RoleId { get; private set; }
        public string NomeRole { get; private set; }

        #region LISTA CLASS MAPPING

        public IEnumerable<UserRoles> UserRoles { get; set; }
        public IEnumerable<ModuloSistemaRoles> ModuloSistemaRoleses { get; set; }

        #endregion
    }
}
