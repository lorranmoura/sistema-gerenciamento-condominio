﻿using System.Collections.Generic;
namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class PlanoDeContas
    {
        public PlanoDeContas(string nomeItem, string debitoCredito, string situacao, int condominioId)
        {
            NomeItem = nomeItem;
            DebitoCredito = debitoCredito;
            PlanoDeContasId = 0;
            Situacao = situacao;
            CondominioId = condominioId;
            Condominio = null;
        }

        public string NomeItem { get; private set; }
        public string DebitoCredito { get; private set; }
        public int PlanoDeContasId { get; private set; }
        public string Situacao { get; private set; }
        public int CondominioId { get; private set; }
        public Condominio Condominio { get; private set; }
        public virtual IEnumerable<ContasReceber> ContasRecebers { get; set; }
        public virtual IEnumerable<ContasPagar> ContasPagars { get; set; }
    }
}
