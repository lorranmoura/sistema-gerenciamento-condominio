﻿using System.Collections.Generic;
using SistemaGerenciamentoCondominio.Dominio.Enum;
using SistemaGerenciamentoCondominio.Dominio.Scopes;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class Condominio
    {
        protected Condominio()
        {

        }
        public Condominio(string nomeCondominio, string endereco, string cidade, string estado, string predominante, string bairro, TipoCondominio tipoCondominio, string telefone, string cnpj, string resumo)
        {
            NomeCondominio = nomeCondominio;
            Endereco = endereco;
            Cidade = cidade;
            Estado = estado;
            Predominante = predominante;
            TipoCondominio = tipoCondominio;
            Bairro = bairro;
            Telefone = telefone;
            Cnpj = cnpj;
            Resumo = resumo;
            CondominioId = 0;
        }
        public int CondominioId { get; private set; }
        public string NomeCondominio { get; private set; }
        public string Telefone { get; private set; }
        public string Predominante { get; private set; }
        public string Cnpj { get; private set; }
        public string Estado { get; private set; }
        public string Cidade { get; private set; }
        public string Cep { get; set; }
        public string Bairro { get; private set; }
        public TipoCondominio TipoCondominio { get; private set; }
        public string Resumo { get; private set; }
        public string Endereco { get; private set; }


        public virtual IEnumerable<Unidade> Unidades { get; set; }
        public virtual IEnumerable<PessoaLocatarioProprietario> PessoaLocatarioProprietarios { get; set; }
        public virtual IEnumerable<User> Users { get; set; }
        public virtual IEnumerable<UserRoles> UserRoles { get; set; }
        public virtual IEnumerable<SmsCobranca> SmsCobrancas { get; set; }
        public virtual IEnumerable<Representantes> Representanteses { get; set; }
        public virtual IEnumerable<PlanoDeContas> PlanoDeContases { get; set; }
        public virtual IEnumerable<Morador> Moradors { get; set; }
        public virtual IEnumerable<ContasPagar> ContasPagars { get; set; }
        public virtual IEnumerable<ContasReceber> ContasRecebers { get; set; }
        public virtual IEnumerable<ContaCorrente> ContaCorrentes { get; set; }
        public virtual IEnumerable<Baixa> Baixas { get; set; }

        public void ValidarCondominio()
        {
            if (!this.CondominioIsValid())
                return;
        }
        public void AtualizarCondominio(Condominio condominio)
        {
            if (!this.UpdateCondominioIsValid())
                return;
        }
    }
}
