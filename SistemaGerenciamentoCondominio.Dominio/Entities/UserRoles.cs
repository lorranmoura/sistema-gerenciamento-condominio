﻿
namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class UserRoles
    {
        public UserRoles(int userRolesId, int userId, User user, int idRole, int roleId, int condominioId, string descricao)
        {
            UserRolesId = userRolesId;
            UserId = userId;
            User = user;
            IdRole = idRole;
            Role = null;
            RoleId = roleId;
            CondominioId = condominioId;
            Condominio = null;
            Descricao = descricao;
        }

        public int UserRolesId { get; private set; }
        public int UserId { get; private set; }
        public User User { get; private set; }
        public int IdRole { get; private set; }
        public Role Role { get; private set; }
        public int RoleId { get; private set; }
        public int CondominioId { get; private set; }
        public Condominio Condominio { get; private set; }
        public string Descricao { get; private set; }
    }
}
