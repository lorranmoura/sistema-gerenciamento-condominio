﻿
using System;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class ContasPagar
    {
        public ContasPagar(string nomeFornecedor, DateTime data, decimal valor, int condominioId, int fornecedorId, int planoDeContasId, string nomePlanoDeContas)
        {
            ContasPagarId = 0;
            NomeFornecedor = nomeFornecedor;
            Data = data;
            Valor = valor;
            Condominio = null;
            CondominioId = condominioId;
            Fornecedor = null;
            FornecedorId = fornecedorId;
            PlanoDeContas = null;
            PlanoDeContasId = planoDeContasId;
            NomePlanoDeContas = nomePlanoDeContas;
        }

        public int ContasPagarId { get; private set; }

        public string NomeFornecedor { get; private set; }
        public string NomePlanoDeContas { get; private set; }
        public DateTime Data { get; private set; }
        public decimal Valor { get; private set; }

        public Condominio Condominio { get; private set; }
        public int CondominioId { get; private set; }
        public Fornecedor Fornecedor { get; private set; }
        public int FornecedorId { get; private set; }
        public PlanoDeContas PlanoDeContas { get; private set; }
        public int PlanoDeContasId { get; private set; }
    }
}
