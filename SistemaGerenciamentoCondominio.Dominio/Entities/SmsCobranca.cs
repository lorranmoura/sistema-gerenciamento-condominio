﻿
namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class SmsCobranca
    {
        public SmsCobranca(string messagemBole, int dias, string nome, int condominioId, char principal)
        {
            SmsCobrancaId = 0;
            MessagemBole = messagemBole;
            Dias = dias;
            Nome = nome;
            CondominioId = condominioId;
            Condominio = null;
            Principal = principal;
        }

        public int SmsCobrancaId { get; private set; }
        public string MessagemBole { get; private set; }
        public int Dias { get; private set; }
        public string Nome { get; private set; }
        public int CondominioId { get; private set; }
        public Condominio Condominio { get; private set; }
        public char Principal { get; private set; }
    }
}
