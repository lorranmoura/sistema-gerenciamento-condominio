﻿
using System.Collections.Generic;
using SistemaGerenciamentoCondominio.Dominio.Enum;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class PessoaLocatarioProprietario
    {
        public Condominio Condominio { get; set; }
        public int CondominioId { get; set; }
        public int PessoaLocatarioProprietarioId { get; set; }
        public string NomePessoa { get; set; }
        public TipoUser TipoPessoa { get; set; }


        #region Dados Pessoais
        public string NacionaliadeDadosPessoais { get; set; }
        public string DataNascDadosPessoais { get; set; }
        public string EstadoCivilDadosPessoais { get; set; }
        public string IdentidadeDadosPessoais { get; set; }
        public string CpfCnpjDadosPessoais { get; set; }
        public int DiabaseVencimentoDadosPessoais { get; set; }
        public string ObservacoesDadosPessoais { get; set; }
        #endregion
        #region Dados de Endereco ABA DE COBRANÇA
        public string EnderecoCobranca { get; set; }
        public string BairroCobranca { get; set; }
        public string CidadeCobranca { get; set; }
        public string EstadoCobranca { get; set; }
        public string CepCobranca { get; set; }
        public string TelefoneCobranca { get; set; }
        public string FaxCobranca { get; set; }
        public string CelularConbranca { get; set; }
        public string EmailCobranca { get; set; }
        #endregion
        #region Dados de Endereco ABA DE RESIDENCIAL
        public string EnderecoResidencial { get; set; }
        public string BairroResidencial { get; set; }
        public string CidadeResidencial { get; set; }
        public string EstadoResidencial { get; set; }
        public string CepResidencial { get; set; }
        public string TelefoneResidencial { get; set; }
        public string FaxResidencial { get; set; }
        public string CelularResidencial { get; set; }
        public string EmailResidencial { get; set; }
        #endregion
        #region Dados de Endereco ABA DE COMERICIAL
        public string EnderecoComercial { get; set; }
        public string BairroComercial { get; set; }
        public string CidadeComercial { get; set; }
        public string EstadoComercial { get; set; }
        public string CepComercial { get; set; }
        public string TelefoneComercial { get; set; }
        public string FaxComercial { get; set; }
        public string CelularComercial { get; set; }
        public string EmailComercial { get; set; }
        #endregion
        #region Dados Bancarios
        public string BancoBancarios { get; set; }
        public int ContaBancarios { get; set; }
        public int AgenciaBancarios { get; set; }
        #endregion

        #region LISTAS CLASS MAPPING
        public virtual IEnumerable<Unidade> Unidades { get; set; }
        public virtual IEnumerable<User> Users { get; set; }
        public virtual IEnumerable<Morador> Moradors { get; set; }
        public virtual IEnumerable<ContasReceber> ContasRecebers { get; set; }
        public virtual IEnumerable<Baixa> Baixas { get; set; }
        #endregion
    }
}
