﻿using System;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class NotificacaoAll
    {
        public NotificacaoAll( DateTime dataNotificacao, string descricao, bool visualizado, int notificacaoId, int userId)
        {
            NotificacaoAllId = 0;
            DataNotificacao = dataNotificacao;
            Descricao = descricao;
            Visualizado = visualizado;
            Notificacao = null;
            NotificacaoId = notificacaoId;
            User = null;
            UserId = userId;
        }

        public int NotificacaoAllId { get; private set; }
        public DateTime DataNotificacao { get; private set; }
        public string Descricao { get; private set; }
        public bool Visualizado { get; private set; }

        public Notificacao Notificacao { get; private set; }
        public int NotificacaoId { get; private set; }
        public User User { get; private set; }
        public int UserId { get; private set; }

    }
}
