﻿
using System.Collections.Generic;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class Unidade
    {
        public Unidade(string tipoUnidade, string cpfCnpj, string telefone, string nomeLocatario, int pessoaLocatarioProprietarioId, string nomeProprietarioo, int condominioId, string cnpjCpfLocatario, string telefoneLocaratario)
        {
            UnidadeId = 0;
            TipoUnidade = tipoUnidade;
            CpfCnpj = cpfCnpj;
            Telefone = telefone;
            NomeLocatario = nomeLocatario;
            PessoaLocatarioProprietarioId = pessoaLocatarioProprietarioId;
            PessoaLocatarioProprietario = null;
            NomeProprietarioo = nomeProprietarioo;
            Condominio = null;
            CondominioId = condominioId;
            CnpjCpfLocatario = cnpjCpfLocatario;
            TelefoneLocaratario = telefoneLocaratario;
        }

        public int UnidadeId { get; private set; }
        public string TipoUnidade { get; private set; }
        public string CpfCnpj { get; private set; }
        public string Telefone { get; private set; }
        public string NomeLocatario { get; private set; }

        public int PessoaLocatarioProprietarioId { get; private set; }
        public PessoaLocatarioProprietario PessoaLocatarioProprietario { get; private set; }
        public string NomeProprietarioo { get; private set; }
        public Condominio Condominio { get; private set; }
        public int CondominioId { get; private set; }
        public string CnpjCpfLocatario { get; private set; }
        public string TelefoneLocaratario { get; private set; }

        public virtual IEnumerable<ContasReceber> ContasReceberS { get; set; }
        public virtual IEnumerable<Unidade> Unidades { get; set; }
        public virtual IEnumerable<Baixa> Baixas { get; set; }
    }
}
