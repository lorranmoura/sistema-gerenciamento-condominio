﻿using System;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class Baixa
    {
        public Baixa(string sacado, decimal valor, DateTime dataBaixa, int condominio, int unidade, int boleto, int pessoaLocatarioProprietario)
        {
            Sacado = sacado;
            Valor = valor;
            DataBaixa = dataBaixa;
            CondominioId = condominio;
            UnidadeId = unidade;
            BoletosId = boleto;
            PessoaLocatarioProprietarioId = pessoaLocatarioProprietario;
            BaixaId = 0;
            Boletos = null;
            Unidade = null;
            Condominio = null;
            PessoaLocatarioProprietario = null;
        }
        public int BaixaId { get; private set; }
        public string Sacado { get; private set; }
        public decimal Valor { get; private set; }
        public DateTime DataBaixa { get; private set; }

        public Condominio Condominio { get; private set; }
        public int CondominioId { get; private set; }


        public Unidade Unidade { get; private set; }
        public int UnidadeId { get; private set; }


        public Boletos Boletos { get; private set; }
        public int BoletosId { get; private set; }

        public PessoaLocatarioProprietario PessoaLocatarioProprietario { get; private set; }
        public int PessoaLocatarioProprietarioId { get; private set; }
    }
}
