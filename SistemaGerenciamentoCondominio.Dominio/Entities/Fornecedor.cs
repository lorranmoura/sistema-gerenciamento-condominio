﻿using System.Collections.Generic;
using SistemaGerenciamentoCondominio.Dominio.Enum;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class Fornecedor
    {
        public Fornecedor(string nomeFornecedor, string descricao, TipoFornecedor tipoFornecedor)
        {
            FornecedorId = 0;
            NomeFornecedor = nomeFornecedor;
            Descricao = descricao;
            TipoFornecedor = tipoFornecedor;
        }

        public int FornecedorId { get;private set; }
        public string NomeFornecedor { get; private set; }
        public string Descricao { get; private set; }
        public TipoFornecedor TipoFornecedor { get; private set; }

        public virtual IEnumerable<ContasPagar> ContasPagars { get; set; }
    }
}
