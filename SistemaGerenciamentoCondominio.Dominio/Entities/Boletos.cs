﻿using System.Collections.Generic;
using SistemaGerenciamentoCondominio.Dominio.Enum;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class Boletos
    {
        public Boletos(string sacado, StatusBoleto statusBoleto, int unidade, int pessoaLocatariaProprietaria)
        {
            BoletosId = 0;
            Sacado = sacado;
            Status = statusBoleto;
            UnidadeId = unidade;
            PessoaLocatarioProprietarioId = pessoaLocatariaProprietaria;
            Unidade = null;
            PessoaLocatarioProprietario = null;
        }
        public int BoletosId { get; private set; }
        public string Sacado { get; private set; }
        public StatusBoleto Status { get; private set; }

        public Unidade Unidade { get; private set; }
        public int UnidadeId { get; private set; }

        public PessoaLocatarioProprietario PessoaLocatarioProprietario { get; private set; }
        public int PessoaLocatarioProprietarioId { get; private set; }


        public virtual IEnumerable<Baixa> Baixas { get; set; }
    }
}
