﻿
namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class ModuloSistemaRoles
    {
        public ModuloSistemaRoles(int roleId, int moduloSistemaId)
        {
            Role = null;
            RoleId = roleId;
            ModuloSistemaId = moduloSistemaId;
            ModuloSistemaRolesId = 0;
            ModuloSistema = null;
        }

        public int ModuloSistemaRolesId { get; private set; }

        public Role Role { get; private set; }
        public int RoleId { get; private set; }
        public ModuloSistema ModuloSistema { get; private set; }
        public int ModuloSistemaId { get; private set; }

    }
}
