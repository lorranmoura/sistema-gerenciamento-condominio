﻿
using SistemaGerenciamentoCondominio.Dominio.Enum;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class Morador
    {
        public Morador(string nome, int pessoaLocatarioProprietarioId, TipoUser tipoUser, int tipoUserId, int condominioId)
        {
            MoradorId = 0;
            Nome = nome;
            PessoaLocatarioProprietario = null;
            PessoaLocatarioProprietarioId = pessoaLocatarioProprietarioId;
            TipoUser = tipoUser;
            TipoUserId = tipoUserId;
            Condominio = null;
            CondominioId = condominioId;
        }

        public int MoradorId { get; private set; }
        public string Nome { get; private set; }

        public PessoaLocatarioProprietario PessoaLocatarioProprietario { get; private set; }
        public int PessoaLocatarioProprietarioId { get; private set; }
        public TipoUser TipoUser { get; private set; }
        public int TipoUserId { get; private set; }
        public Condominio Condominio { get; private set; }
        public int CondominioId { get; private set; }
    }
}
