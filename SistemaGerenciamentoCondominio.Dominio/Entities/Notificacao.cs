﻿using System.Collections.Generic;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class Notificacao
    {
        public Notificacao(string decricao)
        {
            NotificacaoId = 0;
            Decricao = decricao;
        }

        public int NotificacaoId { get;private set; }
        public string Decricao { get; private set; }

        public virtual IEnumerable<NotificacaoAll> NotificacaoAlls { get; set; }
    }
}
