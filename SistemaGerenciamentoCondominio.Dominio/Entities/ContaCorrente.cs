﻿namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class ContaCorrente
    {
        public ContaCorrente(string banco, string agencia, string conta, string natureza, decimal saldo, string referencia, int carteira, int condominioId)
        {
            ContaCorrenteId = 0;
            Banco = banco;
            Agencia = agencia;
            Conta = conta;
            Natureza = natureza;
            Saldo = saldo;
            Referencia = referencia;
            Carteira = carteira;
            CondominioId = condominioId;
            Condominio = null;
        }

        public int ContaCorrenteId { get; private set; }
        public string Banco { get; private set; }
        public string Agencia { get; private set; }
        public string Conta { get; private set; }
        public string Natureza { get; private set; }
        public decimal Saldo { get; private set; }
        public string Referencia { get; private set; }
        public int Carteira { get; private set; }


        public int CondominioId { get; private set; }
        public Condominio Condominio { get; private set; }
    }
}
