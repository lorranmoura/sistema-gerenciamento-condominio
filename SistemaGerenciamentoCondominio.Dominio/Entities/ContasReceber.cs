﻿using System;
namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class ContasReceber
    {
        public ContasReceber(string nomeItem, DateTime data, string vencimento, decimal valor, int pessoaLocatarioProprietarioId, int condominioId, int unidadeId, int planoDeContasId)
        {
            ContasReceberId = 0;
            NomeItem = nomeItem;
            Data = data;
            Vencimento = vencimento;
            Valor = valor;
            PessoaLocatarioProprietario = null;
            PessoaLocatarioProprietarioId = pessoaLocatarioProprietarioId;
            Condominio = null;
            CondominioId = condominioId;
            Unidade = null;
            UnidadeId = unidadeId;
            PlanoDeContas = null;
            PlanoDeContasId = planoDeContasId;
        }

        public int ContasReceberId { get; private set; }
        public string NomeItem { get; private set; }
        public DateTime Data { get; private set; }
        public string Vencimento { get; private set; }
        public decimal Valor { get; private set; }



        public PessoaLocatarioProprietario PessoaLocatarioProprietario { get; private set; }
        public int PessoaLocatarioProprietarioId { get; private set; }
        public Condominio Condominio { get; private set; }
        public int CondominioId { get; private set; }
        public Unidade Unidade { get; private set; }
        public int UnidadeId { get; private set; }
        public PlanoDeContas PlanoDeContas { get; private set; }
        public int PlanoDeContasId { get; private set; }
    }
}
