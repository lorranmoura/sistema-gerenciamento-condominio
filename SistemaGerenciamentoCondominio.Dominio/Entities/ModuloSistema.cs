﻿
using System.Collections.Generic;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class ModuloSistema
    {
        public ModuloSistema(string nomeModulo)
        {
            ModuloSistemaId = 0;
            NomeModulo = nomeModulo;
        }

        public int ModuloSistemaId { get; private set; }
        public string NomeModulo { get; private set; }

        public virtual IEnumerable<ModuloSistemaRoles> ModuloSistemaRoleses { get; set; }
    }
}
