﻿using System;
using System.Collections.Generic;
using Condominio.SharedKernel.Helpers;
using SistemaGerenciamentoCondominio.Dominio.Enum;
using SistemaGerenciamentoCondominio.Dominio.Scopes;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class User
    {
        protected User() { }
        public User(string nome, string passaword, int condominioId, string email, DateTime date, int pessoaLocatarioProprietarioId, TipoUser tipoUser)
        {
            UserId = 0;
            Nome = nome;
            Passaword = StringHelper.Encrypt(passaword);
            Condominio = null;
            CondominioId = condominioId;
            Email = email;
            Date = date;
            if (pessoaLocatarioProprietarioId > 0)
                PessoaLocatarioProprietarioId = pessoaLocatarioProprietarioId;
            PessoaLocatarioProprietario = null;
            TipoUser = tipoUser;
        }

        public int UserId { get; private set; }
        public string Nome { get; private set; }
        public string Passaword { get; private set; }
        public Condominio Condominio { get; private set; }
        public int CondominioId { get; private set; }
        public string Email { get; private set; }
        public DateTime Date { get; private set; }
        public int PessoaLocatarioProprietarioId { get; private set; }
        public PessoaLocatarioProprietario PessoaLocatarioProprietario { get; private set; }
        public TipoUser TipoUser { get; private set; }

        #region LISTA CLASS MAPPING

        public IEnumerable<UserRoles> UserRoles { get; set; }
        public IEnumerable<NotificacaoAll> NotificacaoAlls { get; set; }
        #endregion

        public void ValidarUser()
        {
            if (!this.UserIsValid())
                return;
        }
    }
}
