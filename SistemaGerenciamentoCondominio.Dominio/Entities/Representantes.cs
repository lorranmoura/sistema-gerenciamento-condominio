﻿using System;

namespace SistemaGerenciamentoCondominio.Dominio.Entities
{
    public class Representantes
    {
        public Representantes(int condominioId,  string cargo, string nome, DateTime periodoVigenciaInicio, DateTime periodoVigenciaFim)
        {
            Condominio = null;
            CondominioId = condominioId;
            RepresentantesId = 0;
            Cargo = cargo;
            Nome = nome;
            PeriodoVigenciaInicio = periodoVigenciaInicio;
            PeriodoVigenciaFim = periodoVigenciaFim;
        }

        public Condominio Condominio { get; private set; }
        public int CondominioId { get; private set; }
        public int RepresentantesId { get; private set; }
        public string Cargo { get; private set; }
        public string Nome { get; private set; }
        public DateTime PeriodoVigenciaInicio { get; private set; }
        public DateTime PeriodoVigenciaFim { get; private set; }
    }
}
