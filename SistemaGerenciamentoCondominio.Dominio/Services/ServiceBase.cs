﻿using System;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Services;

namespace SistemaGerenciamentoCondominio.Dominio.Services
{
    public class ServiceBase<TEntity> : IDisposable, IServiceBase<TEntity> where TEntity : class
    {
        private readonly IRepositoryBase<TEntity> _repositoryBase;

        public ServiceBase(IRepositoryBase<TEntity> repositoryBase)
        {
            _repositoryBase = repositoryBase;
        }

        public void Adcionar(TEntity obj)
        {
            _repositoryBase.Adcionar(obj);
        }

        public TEntity BuscaPorId(int id)
        {
          return  _repositoryBase.BuscaPorId(id);
        }

        public System.Collections.Generic.IEnumerable<TEntity> BuscarTodos()
        {
            return _repositoryBase.BuscarTodos();
        }

        public void Atualizar(TEntity obj)
        {
           _repositoryBase.Atualizar(obj);
        }

        public void Remover(TEntity obj)
        {
           _repositoryBase.Remover(obj);
        }

        public void Dispose()
        {
           _repositoryBase.Dispose();
        }
    }
}
