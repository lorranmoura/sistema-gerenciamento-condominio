﻿using SistemaGerenciamentoCondominio.Dominio.Entities;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Services;

namespace SistemaGerenciamentoCondominio.Dominio.Services
{
    public class CondominioService : ServiceBase<Entities.Condominio>, ICondominioService
    {
        private readonly ICondominioRepository _condominioRepository;

        public CondominioService(ICondominioRepository condominioRepository)
            : base(condominioRepository)
        {
            _condominioRepository = condominioRepository;
        }
    }
}
