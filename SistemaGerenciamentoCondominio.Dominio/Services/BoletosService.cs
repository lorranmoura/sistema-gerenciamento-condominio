﻿using SistemaGerenciamentoCondominio.Dominio.Entities;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Services;

namespace SistemaGerenciamentoCondominio.Dominio.Services
{
    public class BoletosService : ServiceBase<Boletos>, IBoletosService
    {
        private readonly IBoletosRepository _boletosRepository;

        public BoletosService(IBoletosRepository boletosRepository) : base(boletosRepository)
        {
            _boletosRepository = boletosRepository;
        }
    }
}
