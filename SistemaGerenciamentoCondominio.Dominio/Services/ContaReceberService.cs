﻿using SistemaGerenciamentoCondominio.Dominio.Entities;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Services;

namespace SistemaGerenciamentoCondominio.Dominio.Services
{
    public class ContaReceberService : ServiceBase<ContasReceber>, IContaReceberService
    {
        public readonly IContaReceberRepository _ContaReceberRepository;
        public ContaReceberService(IContaReceberRepository contaReceberRepository) : base(contaReceberRepository)
        {
            _ContaReceberRepository = contaReceberRepository;
        }
    }
}
