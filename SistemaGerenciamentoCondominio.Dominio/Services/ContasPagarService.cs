﻿
using SistemaGerenciamentoCondominio.Dominio.Entities;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Services;

namespace SistemaGerenciamentoCondominio.Dominio.Services
{
    public class ContasPagarService : ServiceBase<ContasPagar>, IContasPagarService
    {
        public readonly IContasPagarRepository _contasPagarRepository;

        public ContasPagarService(IContasPagarRepository contasPagarRepository) : base(contasPagarRepository)
        {
            _contasPagarRepository = contasPagarRepository;
        }
    }
}
