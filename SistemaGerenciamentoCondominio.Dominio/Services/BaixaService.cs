﻿using SistemaGerenciamentoCondominio.Dominio.Entities;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Services;


namespace SistemaGerenciamentoCondominio.Dominio.Services
{
    public class BaixaService : ServiceBase<Baixa>, IBaixaService
    {
       

        private readonly IBaixaRepository _clienteRepository;

        public BaixaService(IBaixaRepository baixaRepository)
            : base(baixaRepository)
        {
            _clienteRepository = baixaRepository;
        }
    }
}
