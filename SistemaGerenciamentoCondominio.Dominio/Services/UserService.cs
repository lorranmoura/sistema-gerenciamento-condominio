﻿using SistemaGerenciamentoCondominio.Dominio.Entities;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Services;

namespace SistemaGerenciamentoCondominio.Dominio.Services
{
    public class UserService : ServiceBase<User>, IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository repositoryBase) : base(repositoryBase)
        {
            _userRepository = repositoryBase;
        }

        public User Autenticacao(string userName, string password)
        {
            return _userRepository.Autenticacao(userName, password);
        }

       
    }
}
