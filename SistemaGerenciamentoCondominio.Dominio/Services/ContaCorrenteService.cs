﻿using System.Runtime.Remoting.Proxies;
using SistemaGerenciamentoCondominio.Dominio.Entities;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Services;

namespace SistemaGerenciamentoCondominio.Dominio.Services
{
    public class ContaCorrenteService : ServiceBase<ContaCorrente>, IContaCorrenteService
    {
        private readonly IContaCorrenteRepository _contaCorrenteRepository;

        public ContaCorrenteService(IContaCorrenteRepository contaCorrenteRepository) : base(contaCorrenteRepository)
        {
            _contaCorrenteRepository = contaCorrenteRepository;
        }
    }
}
