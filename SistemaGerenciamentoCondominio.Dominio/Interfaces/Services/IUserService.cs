﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Services
{
    public interface IUserService : IServiceBase<User>
    {
        User Autenticacao(string email, string password);
    }
}
