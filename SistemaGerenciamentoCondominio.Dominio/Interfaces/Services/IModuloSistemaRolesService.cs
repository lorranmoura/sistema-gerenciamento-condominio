﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Services
{
    public interface IModuloSistemaRolesService : IServiceBase<ModuloSistemaRoles>
    {
    }
}
