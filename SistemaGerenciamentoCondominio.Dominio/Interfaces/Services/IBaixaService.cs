﻿using SistemaGerenciamentoCondominio.Dominio.Entities;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Services
{
    public interface IBaixaService : IServiceBase<Baixa>
    {
    }
}
