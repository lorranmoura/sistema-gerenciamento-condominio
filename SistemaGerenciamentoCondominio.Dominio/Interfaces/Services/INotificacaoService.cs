﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Services
{
    public interface INotificacaoService : IServiceBase<Notificacao>
    {
    }
}
