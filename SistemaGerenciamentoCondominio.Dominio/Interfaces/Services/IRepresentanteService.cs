﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Services
{
    public interface IRepresentanteService : IServiceBase<Representantes>
    {
    }
}
