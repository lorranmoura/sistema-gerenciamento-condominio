﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories
{
    public interface IBaixaRepository:IRepositoryBase<Baixa>
    {
    }
}
