﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        User Autenticacao(string userName, string password);
    }
}
