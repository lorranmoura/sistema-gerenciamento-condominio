﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories
{
    public interface IUserRolesRepository : IRepositoryBase<UserRoles>
    {
    }
}
