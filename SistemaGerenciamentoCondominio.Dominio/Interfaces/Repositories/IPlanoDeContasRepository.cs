﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories
{
    public interface IPlanoDeContasRepository:IRepositoryBase<PlanoDeContas>
    {
    }
}
