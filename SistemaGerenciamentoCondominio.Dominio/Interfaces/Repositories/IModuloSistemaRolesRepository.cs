﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories
{
    public interface IModuloSistemaRolesRepository:IRepositoryBase<ModuloSistemaRoles>
    {
    }
}
