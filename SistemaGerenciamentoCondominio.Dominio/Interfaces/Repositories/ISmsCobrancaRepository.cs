﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories
{
    public interface ISmsCobrancaRepository : IRepositoryBase<SmsCobranca>
    {
    }
}
