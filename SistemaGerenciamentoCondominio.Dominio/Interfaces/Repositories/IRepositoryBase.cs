﻿using System.Collections.Generic;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories
{
    public interface IRepositoryBase<TEntity> where TEntity : class //CRUD
    {
        void Adcionar(TEntity obj);
        TEntity BuscaPorId(int id);
        IEnumerable<TEntity> BuscarTodos();
        void Atualizar(TEntity obj);
        void Remover(TEntity obj);
        void Dispose();
    }
}
