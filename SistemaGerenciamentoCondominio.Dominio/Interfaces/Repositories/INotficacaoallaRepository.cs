﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories
{
    public interface INotficacaoallaRepository : IRepositoryBase<NotificacaoAll>
    {
    }
}
