﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories
{
   public interface ICondominioRepository :IRepositoryBase<Entities.Condominio>
    {

    }
}
