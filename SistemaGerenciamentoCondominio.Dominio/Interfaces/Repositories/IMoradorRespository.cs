﻿using SistemaGerenciamentoCondominio.Dominio.Entities;

namespace SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories
{
    public interface IMoradorRespository : IRepositoryBase<Morador>
    {
    }
}
