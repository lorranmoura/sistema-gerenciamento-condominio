﻿using System;
using System.Linq.Expressions;

namespace SistemaGerenciamentoCondominio.Dominio.Specs
{
    public static class CondominioSpecs
    {
        public static Expression<Func<Entities.Condominio, bool>> BuscarCondominioPeloNome(string nome)
        {
            return x => x.NomeCondominio == nome;
        }
    }
}
