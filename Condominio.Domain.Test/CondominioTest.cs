﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SistemaGerenciamentoCondominio.Dominio.Enum;
using SistemaGerenciamentoCondominio.Dominio.Scopes;

namespace Condominio.Domain.Test
{
    [TestClass]
    public class CondominioTest
    {
        [TestMethod]
        [TestCategory("Condominio")]
        public void VerificaSeCondominoEValiodo()
        {
            var condominio = new SistemaGerenciamentoCondominio.Dominio.Entities.Condominio("Condominio Teste", "teste",
                "teste", "teste", "teste", "teste", TipoCondominio.Horizontal, "teste", "teste", "");
            //condominio.Register();
            Assert.AreEqual(true, condominio.CondominioIsValid());
        }
    }
}
