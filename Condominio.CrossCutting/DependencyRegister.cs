﻿using Condominio.SharedKernel;
using Condominio.SharedKernel.Events;
using Microsoft.Practices.Unity;
using SistemaGerenciamentoCond.Application;
using SistemaGerenciamentoCond.Application.Interface;
using SistemaGerenciamentoCond.Infra.Data.Repositories;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Repositories;
using SistemaGerenciamentoCondominio.Dominio.Interfaces.Services;
using SistemaGerenciamentoCondominio.Dominio.Services;

namespace Condominio.CrossCutting
{
    public static class DependencyRegister
    {
        public static void Register(UnityContainer container)
        {
            container.RegisterType(typeof(IAppServiceBase<>), typeof(AppServiceBase<>));
            container.RegisterType<ICondominioAppService, CondominioAppService>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserAppService, UserAppService>(new HierarchicalLifetimeManager());

            container.RegisterType(typeof(IServiceBase<>), typeof(ServiceBase<>));
            container.RegisterType<ICondominioService, CondominioService>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserService, UserService>(new HierarchicalLifetimeManager());

            container.RegisterType(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));
            container.RegisterType<ICondominioRepository, CondominioRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserRepository, UserRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<IHandler<DomainNotification>, DomainNotificationHandler>(new HierarchicalLifetimeManager());
        }
    }
}
