﻿using System;
using System.Collections.Generic;


namespace Condominio.SharedKernel
{
    public interface IContainer
    {
        T GetService<T>();
        object GetService(Type serviceType);
        IEnumerable<object> GetServices(Type serviceType);
        IEnumerable<T> GetServices<T>();
    }
}
