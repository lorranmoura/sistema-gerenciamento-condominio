﻿using System;
using System.Collections.Generic;
using Condominio.SharedKernel.Events.Contracts;

namespace Condominio.SharedKernel
{
    public interface IHandler<T> : IDisposable where T : IDomainEvent
    {
        void Handle(T args);
        IEnumerable<T> Notificacao();
        bool HasNotifications();
    }
}
