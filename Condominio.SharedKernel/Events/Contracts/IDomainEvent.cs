﻿using System;

namespace Condominio.SharedKernel.Events.Contracts
{
    public interface IDomainEvent
    {
        DateTime DataOcorrida { get; }
    }
}
