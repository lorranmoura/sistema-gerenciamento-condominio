﻿
using System;
using Condominio.SharedKernel.Events.Contracts;

namespace Condominio.SharedKernel.Events
{
    public class DomainNotification : IDomainEvent
    {
        public string Key { get; private set; }
        public string Value { get; private set; }
        public DateTime DataOcorrida { get; private set; }

        public DomainNotification(string key, string value)
        {
            Key = key;
            Value = value;
            DataOcorrida = DateTime.Now;
        }
    }
}
